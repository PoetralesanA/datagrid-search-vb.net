﻿Public Class Form1
    ' ./INFO
    '- Created by Programmer VB.NET indonesia (Group)
    '- https://www.facebook.com/groups/72258093620
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DgvProp.Dgvset(DataGridView1)
        LoadData()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        ''---------------------- Bagian Ini berfungsi untuk mencari seluruh data ----------------------''
        'Try
        '    Dim getRow As DataGridViewRow = (
        '    From row As DataGridViewRow In DataGridView1.Rows
        '    From c As DataGridViewCell In row.Cells
        '    Where (LCase(c.FormattedValue)).ToString.Contains(LCase(txtSrc.Text)) Select row
        '    ).First

        '    DataGridView1.Rows(getRow.Index).Selected = True
        'Catch ex As Exception
        '    MsgBox(IIf(ex.Message.Contains("Sequence"), "Data tidak ditemukan", ex.Message), MsgBoxStyle.Exclamation, "")
        'End Try
        ''---------------------------------------------------------------------------------------''

        '////////////////////////////////////////////////////////////////////////////////////////////

        ''-------------- Bag. ini Mencari Data Berdasarkan Name Column/Index Column --------------''
        'Try
        '    Dim getRow As DataGridViewRow = (
        '    From row As DataGridViewRow In DataGridView1.Rows
        '    Where LCase(row.Cells(COLUMNINDEX/NAME).FormattedValue).ToString.Contains(LCase(txtSrc.Text)) Select row
        '    ).First

        '    DataGridView1.Rows(getRow.Index).Selected = True
        'Catch ex As Exception
        '    MsgBox(IIf(ex.Message.Contains("Sequence"), "Data tidak ditemukan", ex.Message), MsgBoxStyle.Exclamation, "")
        'End Try
        ''------------------------------------------------------------------------------------------''
    End Sub
#Region "Data"
    Private Sub LoadData()
        Dim data As New List(Of PropertyData)()
        data.Add(New PropertyData() With {
            .nama = "Royko",
            .tanggal_lahir = "02 April 1992",
            .alamat = "Jl. Ujung kulon",
            .no_telp = 628592324234,
            .email = "Erere@gmail.com"
        })
        data.Add(New PropertyData() With {
            .nama = "Sugro",
            .tanggal_lahir = "02 Jan 1990",
            .alamat = "Jl. qwery",
            .no_telp = 894375345,
            .email = "Ckck@gmail.com"
        })
        data.Add(New PropertyData() With {
           .nama = "Supardi",
           .tanggal_lahir = "02 Agus 1999",
           .alamat = "Jl. Mahdah",
           .no_telp = 8947598375,
           .email = "LAKJWD@gmail.com"
       })
        data.Add(New PropertyData() With {
          .nama = "Yuyud",
          .tanggal_lahir = "02 Maret 1989",
          .alamat = "Jl. xmuaster",
          .no_telp = 8947598375,
          .email = "hamsternakal@gmail.com"
      })
        data.Add(New PropertyData() With {
        .nama = "Mustia",
        .tanggal_lahir = "01 Maret 1995",
        .alamat = "Jl. Kamboja",
        .no_telp = 622384798118,
        .email = "nekopoi@gmail.com"
      })
        data.Add(New PropertyData() With {
           .nama = "Rampage",
           .tanggal_lahir = "02 Sept 1990",
           .alamat = "Jl. Asmara",
           .no_telp = 99894375345,
           .email = "rpg@gmail.com"
       })
        DataGridView1.DataSource = data
    End Sub
#End Region
End Class