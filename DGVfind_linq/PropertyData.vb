﻿Public Class PropertyData
    Private _nama As String
    Public Property nama() As String
        Get
            Return _nama
        End Get
        Set(ByVal value As String)
            _nama = value
        End Set
    End Property
    Private _tanggal_lahir As String
    Public Property tanggal_lahir() As String
        Get
            Return _tanggal_lahir
        End Get
        Set(ByVal value As String)
            _tanggal_lahir = value
        End Set
    End Property
    Private _alamat As String
    Public Property alamat() As String
        Get
            Return _alamat
        End Get
        Set(ByVal value As String)
            _alamat = value
        End Set
    End Property
    Private _email As String
    Public Property email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property
    Private _noTelp As Double
    Public Property no_telp() As Double
        Get
            Return _noTelp
        End Get
        Set(ByVal value As Double)
            _noTelp = value
        End Set
    End Property
End Class