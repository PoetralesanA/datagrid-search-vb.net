﻿Module DgvProp
    Public Sub Dgvset(ByVal Grid As DataGridView)
        With Grid
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False
            .MultiSelect = False
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AllowUserToResizeRows = False
            .ReadOnly = True
            MsgBox("Programmer VB.NET Indonesia Group", MsgBoxStyle.Information, "")
        End With
    End Sub
End Module
